/**********************************************************************************************
*
*   LICENSE: MIT
*
*   Copyright (c) 2023 wildandev
*
*   Permission is hereby granted, free of charge, to any person obtaining a copy
*   of this software and associated documentation files (the "Software"), to deal
*   in the Software without restriction, including without limitation the rights
*   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*   copies of the Software, and to permit persons to whom the Software is
*   furnished to do so, subject to the following conditions:
*
*   The above copyright notice and this permission notice shall be included in all
*   copies or substantial portions of the Software.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*   SOFTWARE.
*
**********************************************************************************************/

#include "App.h"

enum class ApplicationStates
{
	MENU = 0,
	GAMEPLAY,
	OPTION
};
ApplicationStates applicationState;

class MenuScreen : public Screen
{
public:
	void Draw() override
	{
		const std::string welcomeText = "Hi! " + _playerName;

		DrawText(welcomeText.c_str(), 40, 40, 20, BLUE);

		DrawCenteredText(120, "Menu", 30, GREEN);
		DrawButton();
	}

private:
	void DrawButton()
	{
		const float buttonX = screenWidth - 250;
		const float buttonY = 150;

		_menuButton.isGameplay = GuiButton({ buttonX, buttonY,      120, 35 }, "Play"  );
		_menuButton.isOption   = GuiButton({ buttonX, buttonY + 45, 120, 35 }, "Option");

		if (_menuButton.isGameplay) applicationState = ApplicationStates::GAMEPLAY;
		if (_menuButton.isOption)   applicationState = ApplicationStates::OPTION;
	}

private:
	struct _MenuButton
	{
		bool isGameplay = 0;
		bool isOption   = 0;
	};
	_MenuButton _menuButton;
	std::string _playerName = FileUtils::LoadPlayerName();
};
MenuScreen menuScreen;

class GameplayScreen : public Screen
{
public:
	void Draw() override
	{
		DrawCenteredText(120, "Gameplay", 30, GREEN);
	}
};
GameplayScreen gameplayScreen;

class OptionScreen : public Screen
{
public:
	void Draw() override
	{
		DrawCenteredText(120, "Option", 30, GREEN);
		DrawText("Press Enter to edit", 50, 200, 20, DARKGRAY);
		
		std::string playerName = "Name: ";
		DrawText(playerName.append(_playerName).c_str(), 50, 240, 20, DARKGRAY);

		if (IsMouseOverRectangle(_textBoxRec)) DrawRectangleRec(_textBoxRec, GRAY);
		else DrawRectangleRec(_textBoxRec, LIGHTGRAY);

		DrawRectangleLines(_textBoxRec.x, _textBoxRec.y, _textBoxRec.width, _textBoxRec.height, DARKGRAY);
		DrawText(_textBoxText, _textBoxRec.x + 5, _textBoxRec.y + 8, 20, MAROON);

		if (_textBoxEditMode)
		{
			DrawText("Press Enter to finish editing", 50, 340, 20, DARKGRAY);
		}

		Update();
	}

private:
	void Update()
	{
		if (IsKeyPressed(KEY_ENTER))
		{
			if (IsMouseOverRectangle(_textBoxRec))
			{
				_textBoxEditMode = !_textBoxEditMode;
			}
		}

		if (_textBoxEditMode)
		{
			int key = GetKeyPressed();
			int keyCount = strlen(_textBoxText);

			if ((key >= 32) && (key <= 125) && (keyCount < MAX_INPUT_CHARS))
			{
				_textBoxText[keyCount] = (char)key;
				_textBoxText[keyCount + 1] = '\0';
			}

			if (IsKeyPressed(KEY_BACKSPACE) && (keyCount > 0))
			{
				_textBoxText[keyCount - 1] = '\0';
			}

			_playerName = _textBoxText;

			FileUtils::SavePlayerName(_playerName);
		}

#ifdef _DEBUG
	std::cout << _textBoxText << std::endl;
#endif

	}

private:
	char _textBoxText[MAX_INPUT_CHARS + 1] = "";
	bool _textBoxEditMode = false;
	Rectangle _textBoxRec = { 50, 280, 220, 40 };
	std::string _playerName = FileUtils::LoadPlayerName();
};
OptionScreen optionScreen;

void App::Start()
{
	InitWindow(screenWidth, screenHeight, "");
}

void App::Update()
{
	BeginDrawing();
	ClearBackground(WHITE);
	UpdateScreen();
	EndDrawing();
}

void App::Free()
{
	CloseWindow();
}

void App::UpdateScreen()
{
	switch (applicationState)
	{
	case ApplicationStates::MENU:
	{
		SetActiveScreen(&menuScreen);
	} break;
	case ApplicationStates::GAMEPLAY:
	{
		SetActiveScreen(&gameplayScreen);
	} break;
	case ApplicationStates::OPTION:
	{
		SetActiveScreen(&optionScreen);
	} break;
	default:
		break;
	}

	DrawScreen();
}

bool IsMouseOverRectangle(Rectangle rec)
{
	Vector2 mousePosition = GetMousePosition();
	return { mousePosition.x >= rec.x && mousePosition.x <= (rec.x + rec.width) &&
		mousePosition.y >= rec.y && mousePosition.y <= (rec.y + rec.height) };
}

int main()
{
	App app;
	app.Start();

	try 
	{
		if (!IsWindowReady()) 
		{
			throw std::runtime_error("Can't initialize the window.");
		}

		while (!WindowShouldClose())
		{
			app.Update();
		}
	}
	catch (const std::exception& e) 
	{
		TraceLog(LOG_WARNING, e.what());
	}
	catch (...) 
	{
		TraceLog(LOG_WARNING, "An unknown error occurred.");
	}

	app.Free();

	return 0;
}