/**********************************************************************************************
*
*   LICENSE: MIT
*
*   Copyright (c) 2023 wildandev
*
*   Permission is hereby granted, free of charge, to any person obtaining a copy
*   of this software and associated documentation files (the "Software"), to deal
*   in the Software without restriction, including without limitation the rights
*   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*   copies of the Software, and to permit persons to whom the Software is
*   furnished to do so, subject to the following conditions:
*
*   The above copyright notice and this permission notice shall be included in all
*   copies or substantial portions of the Software.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*   SOFTWARE.
*
**********************************************************************************************/

#pragma once

#include "raylib.h"
#include "Screen.h"
#include "FileUtils.h"

#include <stdexcept>

#define RAYGUI_IMPLEMENTATION
#include "extras/raygui.h"

#ifdef _DEBUG
#include <iostream>
#endif

#define MAX_INPUT_CHARS 20

class App
{
public:
	void Start();
	void Update();
	void Free();

private:
	void UpdateScreen();
};

static const int screenWidth  = 375;
static const int screenHeight = 568;

bool IsMouseOverRectangle(Rectangle rec);